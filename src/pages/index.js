import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'

const IndexPage = () => (
  <Layout>
    <SEO title="Home" keywords={[`kırık`, `kalpler`, `ağacı`,`agc`,`krk`,`kalp`,`dilek`,`ağacı`,`ağacı`]} />
    <h1>Yapım Aşamasında</h1>
    <p>Yakında açılacak</p>
    <Link to="/about/">Hakkında</Link>
  </Layout>
) 

export default IndexPage
