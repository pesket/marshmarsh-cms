import React from 'react'
import { Link } from 'gatsby'

import Layout from '../components/layout'
import SEO from '../components/seo'

const About = () => (
  <Layout>
    <SEO title="Hakkında" />
    <h1>Hakkında</h1>
    <p></p>
    <Link to="/">Ana sayfaya dön</Link>
  </Layout>
)

export default About
