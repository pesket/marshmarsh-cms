module.exports = {
  siteMetadata: {
    title: `Kırık Kalpler Ağacı - Dileğinizi ağaca asın`,
    description: `Kalbiniz kırıksa dileğinizi ağaca asabilirsiniz. Ağaca astığınız dilekler sayesinde mutlu olabilirsiniz. Mutlu olmak istemez miydiniz?`,
    author: `@cempesket`,
    headerTitle: 'Kırık Kalpler Ağacı'
  },
  plugins: [
    `gatsby-plugin-react-helmet`,
    `gatsby-plugin-netlify-cms`,
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `images`,
        path: `${__dirname}/src/images`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `kirik-kalpler-agaci`,
        short_name: `starter`,
        start_url: `/`,
        background_color: `#663399`,
        theme_color: `#663399`,
        display: `minimal-ui`,
        icon: `src/images/favicon-96x96.png`, // This path is relative to the root of the site.
      },
    },
    // this (optional) plugin enables Progressive Web App + Offline functionality
    // To learn more, visit: https://gatsby.app/offline
    // 'gatsby-plugin-offline',
  ],
}
